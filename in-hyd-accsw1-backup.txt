Building configuration...

Current configuration : 1638 bytes
!
! Last configuration change at 22:32:27 IST Thu Jul 22 2021 by cisco
!
version 15.1
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
service compress-config
!
hostname IN-HYD-ACCSW1
!
boot-start-marker
boot-end-marker
!
!
!
username cisco privilege 15 secret 4 tnhtc92DXBhelxjYk8LWJrPV36S2i4ntXrpb4RFmfqY
no aaa new-model
clock timezone IST 5 30
!
ip cef
!
!
no ip domain-lookup
ip domain-name abc.inc
no ipv6 cef
ipv6 multicast rpf use-bgp
!
!
!
!
!
!
!
!
spanning-tree mode rapid-pvst
spanning-tree extend system-id
!
!
!
!
vlan internal allocation policy ascending
!
! 
!
!
!
!
!
!
!
!
interface Loopback10
 description LoopbackforTest
 no ip address
!
interface Ethernet0/0
 switchport trunk encapsulation dot1q
 switchport mode trunk
 duplex auto
!
interface Ethernet0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 duplex auto
!
interface Ethernet0/2
 duplex auto
!
interface Ethernet0/3
 description MGMTSW-1
 no switchport
 ip address 192.168.1.105 255.255.255.0
!
interface Ethernet1/0
 duplex auto
!
interface Ethernet1/1
 duplex auto
!
interface Ethernet1/2
 switchport access vlan 30
 switchport mode access
 duplex auto
!
interface Ethernet1/3
 duplex auto
!
interface Vlan30
 ip address 192.168.32.100 255.255.255.0
!
router ospf 100
 network 192.168.32.0 0.0.0.255 area 0
!
!
ip http server
!
!
ip access-list extended temp-ansible-acl
 permit ip host 192.168.10.250 any
!
!
!
!
control-plane
!
!
line con 0
 exec-timeout 0 0
 logging synchronous
line aux 0
line vty 0 4
 login local
 transport input ssh
!
ntp server 10.10.11.1
netconf ssh
end