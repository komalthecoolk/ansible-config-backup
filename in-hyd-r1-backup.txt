Building configuration...

Current configuration : 1502 bytes
!
! Last configuration change at 22:32:27 IST Thu Jul 22 2021 by cisco
version 15.2
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname IN-HYD-R1
!
boot-start-marker
boot-end-marker
!
!
!
no aaa new-model
clock timezone IST 5 30
mmi polling-interval 60
no mmi auto-configure
no mmi pvc
mmi snmp-timeout 180
!
!
!
!
!
!
no ip domain lookup
ip domain name abc.in
ip cef
no ipv6 cef
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
!
username cisco privilege 15 secret 4 tnhtc92DXBhelxjYk8LWJrPV36S2i4ntXrpb4RFmfqY
!
redundancy
!
lldp run
!
! 
!
!
!
!
!
!
!
!
!
!
!
interface Loopback10
 description LoopbackforTest
 no ip address
!
interface Ethernet0/0
 no ip address
!
interface Ethernet0/0.10
 encapsulation dot1Q 10
 ip address 10.10.11.1 255.255.255.0
!
interface Ethernet0/1
 no ip address
!
interface Ethernet0/2
 no ip address
 shutdown
!
interface Ethernet0/3
 ip address 192.168.1.101 255.255.255.0
!
router ospf 100
 network 10.10.11.0 0.0.0.255 area 0
 default-information originate
!
ip forward-protocol nd
!
!
no ip http server
ip http secure-server
ip route 0.0.0.0 0.0.0.0 192.168.1.1
!
ip access-list extended temp-ansible-acl
 permit ip host 192.168.1.200 any
 permit ip host 192.168.1.100 any
 permit ip host 192.168.10.250 any
!
!
!
!
control-plane
!
!
!
!
!
!
!
line con 0
 exec-timeout 0 0
 logging synchronous
line aux 0
line vty 0 4
 login local
 transport input ssh
!
ntp master
ntp server 216.239.35.4
!
end